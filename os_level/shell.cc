#include "shell.hh"

int main()
{ std::string input;

  // bedankt, 8 jarig jongetje uit India voor de tutorial op youtube
  char line[80];
  int filedesc = syscall(SYS_open, "config", O_RDONLY, 0755);
  std::string prompt = std::to_string(syscall(SYS_read, filedesc, line, 80));
  prompt = line;

  while(true)
  { std::cout << prompt;                   // Print het prompt
    std::getline(std::cin, input);         // Lees een regel
    if (input == "new_file") new_file();   // Kies de functie
    else if (input == "ls") list();        //   op basis van
    else if (input == "src") src();        //   de invoer
    else if (input == "find") find();
    else if (input == "seek") seek();
    else if (input == "exit") return 0;
    else if (input == "quit") return 0;
    else if (input == "error") return 1;

    if (std::cin.eof()) return 0; } }      // EOF is een exit

void new_file()
{ std::cout << "Geef bestandsnaam: "; 
  std::string fn = "";
  std::getline(std::cin, fn);
  syscall(SYS_creat, fn.c_str(), 0755);
  int fd = syscall(SYS_open, fn.c_str(), O_RDWR, 0755);
  std::cout << "Voer de text in: ";
  std::string text = "";
  std::getline(std::cin, text);
  const char*cstr = text.c_str();
  syscall(SYS_write, fd, cstr, strlen(cstr));
  syscall(SYS_close, fd);
}

void list()
{ 	int PID = syscall(SYS_fork);
	if(PID == 0){
		const char* args[] = {"/bin/ls", "-l", "-a", NULL};
		std::cout << syscall(SYS_execve, args[0], args, NULL);
		exit(0);
	}
	else{
		syscall(SYS_wait4, PID, NULL, NULL, NULL);
	}
}


// Het testbestand zegt dat deze niet werkt, maar als ik in de terminal <$find . | grep "<string>"> invoer, dan krijg ik dezelfde output
// dus ik denk dat het werkt? Maar waarom zouden we dan grep gebruiken in plaats van <find -iname *<string>*>?
void find(){ 	
	std::cout << "Gooi string: "; 
	std::string str = "";
	std::getline(std::cin, str);
	int filedesc[2];
	syscall(SYS_pipe, &filedesc, NULL);
	int PID = syscall(SYS_fork);
	if(PID == 0){
		int PID1 = syscall(SYS_fork);
		if(PID1 == 0){
			syscall(SYS_close, filedesc[0]);
			syscall(SYS_dup2, filedesc[1], 1);
			const char* args[] = { "/usr/bin/find", ".", NULL };
            syscall(SYS_execve, args[0], args, NULL);
		}
		else{
			syscall(SYS_close, filedesc[1]);
            syscall(SYS_dup2, filedesc[0], 0);
            const char* args2[] = { "/bin/grep", str.c_str(), NULL};
            syscall(SYS_execve, args2[0], args2, NULL); 
			syscall(SYS_wait4, PID1, NULL);
		}
	}
	else{  
			syscall(SYS_wait4, PID, NULL);
			return;
		}
}

void seek(){
  std::string file1 = "seek";
  std::string file2 = "loop";
  int seekfile = syscall(SYS_creat, file1.c_str(), 0755);
  int seek = syscall(SYS_open, file1.c_str(), O_RDWR, 0755);
  syscall(SYS_write, seek, "x", 1);
  syscall(SYS_lseek, seek, 5000000, SEEK_END);
  syscall(SYS_write, seek, "x", 1);
  int loopfile = syscall(SYS_creat, file2.c_str(), 0755);  
  int loop = syscall(SYS_open, file2.c_str(), O_RDWR, 0755);
  syscall(SYS_write, loop, "x", 1);
  for (unsigned int i = 0; i < 5000000; i++) {
    syscall(SYS_write, loop, "\0", 1);
  }
  syscall(SYS_write, loop, "x", 1);
}
// bij het schrijven van 'seek' zijn maar 3 stappen nodig
// bij het schrijven van 'loop' zijn er ~10 miljoen nodig
// lseek maakt het dus een hoop sneller


void src() // Voorbeeld: Gebruikt SYS_open en SYS_read om de source van de shell (shell.cc) te printen.
{ int fd = syscall(SYS_open, "shell.cc", O_RDONLY, 0755); // Gebruik de SYS_open call om een bestand te openen.
  char byte[1];                                           // 0755 zorgt dat het bestand de juiste rechten krijgt (leesbaar is).
  while(syscall(SYS_read, fd, byte, 1))                   // Blijf SYS_read herhalen tot het bestand geheel gelezen is,
    std::cout << byte; }                                  //   zet de gelezen byte in "byte" zodat deze geschreven kan worden.


